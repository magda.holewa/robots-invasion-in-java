package invasion;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import robots.*;

import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;

import java.io.File;

/**
 * Klasa odpowiedzialna za obsługę elementów interfejsu graficznego.
 */
public class Controller {
    @FXML
    private Canvas fieldCanvas;
    @FXML
    private Button button;
    @FXML
    private Button saveButton;
    @FXML
    private Label decisionLabel;

    private Field field;
    private static Stage stage;

    public static void setStage(Stage _stage) {
        stage = _stage;
    }

    /**
     * Metoda wywoływana po wciśnięciu przycisku "Generate". Tworzy planszę z losowo rozstawionymi robotami
     * i nadajnikami oraz wyświetla decyzję robota-matki.
     */
    @FXML
    protected void buttonOnClick() {
        field = Field.builder().setWidth(400).setHeight(400)
                .setMother().setRobots(1000).setSignalSources().build();
        drawField();
        saveButton.setDisable(false);
        decisionLabel.setText("Decision: " + (field.mother.isInTriangle() ? "stay" : "escape"));
    }

    /**
     * Rysuje planszę z robotami na podstawie danych z obiektu <em>field</em>.
     *
     */
    protected void drawField() {
        GraphicsContext gc = fieldCanvas.getGraphicsContext2D();
        gc.setFill(Color.WHITE);
        gc.fillRect(0, 0, fieldCanvas.getWidth(), fieldCanvas.getHeight());
        double scale = fieldCanvas.getWidth() / Math.max(field.width, field.height);
        gc.setFill(Color.GREEN);
        for (Robot s : field.mother.getRobots()) {
            gc.fillOval(s.getX() * scale - 2, s.getY() * scale - 2, 4, 4);
        }
        gc.setLineWidth(2.0);
        gc.setFill(Color.BLUE);
        gc.fillOval(field.mother.getX() * scale - 4, field.mother.getY() * scale - 4, 8, 8);
        gc.setStroke(Color.BLUE);
        gc.strokeOval(field.mother.getX() * scale - 8, field.mother.getY() * scale - 8, 16, 16);
        gc.setFill(Color.RED);
        gc.setStroke(Color.RED);
        for (SignalSource s : field.sources) {
            gc.fillOval(s.getX() * scale - 4, s.getY() * scale - 4, 8, 8);
            gc.strokeOval(s.getX() * scale - 8, s.getY() * scale - 8, 16, 16);
        }
    }

    /**
     * Metoda wywoływana po naciśnięciu przycisku "Open file". Wyświetla okno wyboru pliku i tworzy planszę na
     * podstawie danych z wybranego pliku. W przypadku błędu wyświetlane jest okno z powiadomieniem.
     */
    @FXML
    protected void generateFromFile() {
        FileChooser chooser = new FileChooser();
        configureChooser(chooser);
        File file = chooser.showOpenDialog(stage);
        if (file != null) {
            try {
                field = Field.builder().setDataFromFile(file).build();
                drawField();
                decisionLabel.setText("Decision: " + (field.mother.isInTriangle() ? "stay" : "escape"));
            }
            catch (Exception e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                String s = "The file doesn't contain proper data.";
                alert.setContentText(s);
                alert.showAndWait();
            }
        }
    }

    /**
     * Metoda wywoływana po wciśnięciu przycisku "Save file". Wyświetla okno wyboru pliku i zapisuje
     * do niego dane z aktualnie wygenerowanej planszy. W przypadku błędu wyświetlane jest okno z powiadomieniem.
     */
    @FXML
    protected void saveData() {
        FileChooser chooser = new FileChooser();
        configureChooser(chooser);
        File file = chooser.showSaveDialog(stage);
        if (file != null) {
            try {
                field.sendDataToFile(file);
            }
            catch (Exception e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                String s = "Can't write data to file " + file.getPath() + ".";
                alert.setContentText(s);
                alert.showAndWait();
            }
        }

    }

    /**
     * Ustawia parametry dla okna wyboru pliku.
     * @param fileChooser obiekt okna wyboru
     */
    protected void configureChooser(FileChooser fileChooser) {
        fileChooser.setTitle("Choose file");
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
    }
}
