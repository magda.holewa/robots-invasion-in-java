package robots;

import java.util.Random;

/**
 * Klasa reprezentująca źródło sygnału.
 */
public class SignalSource {
    private double x;
    private double y;
    private static double A=1;
    private static double n=1;

    /**
     * Konstruktor parametryczny
     * @param _x współrzędna pozioma
     * @param _y współrzędna pionowa
     */
    public SignalSource(double _x, double _y) {
        x = _x;
        y = _y;
    }

    /**
     * Generuje parametry funkcji sygnału, wspólne dla wszystkich źródeł.
     */
    public static void initFunctionParameters() {
        Random gen = new Random();
        A = 10 + gen.nextDouble()*2; //od 10 do 12
        n = 0.5 + gen.nextDouble(); //0.5-1.5
    }

    /**
     * Zwraca współrzędną poziomą.
     * @return współrzędna pozioma
     */
    public double getX() {
        return x;
    }

    /**
     * Zwraca współrzędną pionową
     * @return współrzędna pionowa
     */
    public double getY() {
        return y;
    }

    /**
     * Oblicza wartość funkcji sygnału w zadanym punkcie.
     * @param posX współrzędna pozioma
     * @param posY współrzędna pionowa
     * @return wartość sygnału
     */
    public double signalValue(double posX, double posY) {
        double d = Math.sqrt(Math.pow(posX - x, 2) + Math.pow(posY-y, 2));
        return A - 10*n*Math.log10(d);
    }
}
