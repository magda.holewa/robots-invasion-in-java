package robots;

/**
 * Klasa wyjątku dla nieprawidłowych operacji na macierzach.
 */
public class MatrixException extends Exception {
    public MatrixException(String message) {
        super(message);
    }
}
