package robots;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * Klasa przechowująca dane na temat planszy i obiektów na niej umieszczonych.
 */
public class Field {
    public int width;
    public int height;
    public Mother mother;
    public SignalSource[] sources;

    /**
     * Konstruktor parametryczny, wywoływany przez obiekt FieldBuilder.
     * @param w szerokość planszy
     * @param h wysokość planszy
     * @param _mother robot-matka
     * @param _robots lista pozostałych robotów
     * @param _sources tablica nadajników
     */
    protected Field(int w, int h, Mother _mother, List<Robot> _robots, SignalSource[] _sources) {
        width= w;
        height = h;
        mother = _mother;
        sources = _sources;
        for (Robot r : _robots) {
            r.receiveSignals(sources);
        }
        mother.receiveSignals(sources);
        mother.setRobotsList(_robots);
    }

    /**
     * Zapisuje dane o pozycjach obiektów do pliku.
     * @param file uchwyt do pliku
     * @throws FileNotFoundException nie można utworzyć strumienia zapisu do pliku
     */
    public void sendDataToFile(File file) throws FileNotFoundException{
        PrintWriter stream = new PrintWriter(file);
        stream.println(width+" "+height);
        for(int i = 0;i < 3;i++){
            stream.println(sources[i].getX()+" "+sources[i].getY());
        }
        stream.println(mother.getX()+" "+mother.getY());
        for(Robot robot: mother.getRobots()){
            stream.println(robot.getX()+" "+robot.getY());
        }
        stream.close();
    }

    /**
     * Tworzy nowy obiekt klasy {@link Field.FieldBuilder}.
     * @return stworzony obiekt
     */
    public static FieldBuilder builder() {
        return new FieldBuilder();
    }

    /**
     * Klasa buildera dla obiektów typu {@link Field}.
     */
    public static class FieldBuilder {
        private static int _width;
        private static int _height;
        private static Mother _mother;
        private static SignalSource[] _sources;
        private static List<Robot> _robots;

        /**
         * Ustawia szerokość planszy.
         * @param width szerokość
         * @return metoda zwraca referencję <em>this</em>
         */
        public FieldBuilder setWidth(int width) {
            _width = width;
            return this;
        }

        /**
         * Ustawia wysokość planszy.
         * @param height wysokość
         * @return metoda zwraca referencję <em>this</em>
         */
        public FieldBuilder setHeight(int height) {
            _height = height;
            return this;
        }

        /**
         * Ustawia robota-matkę, który ma zostać umieszczony w losowym punkcie planszy.
         * @return metoda zwraca referencję <em>this</em>
         */
        public FieldBuilder setMother() {
            Random gen = new Random();
            _mother = new Mother(gen.nextDouble()*_width, gen.nextDouble()*_height);
            return this;
        }

        /**
         * Tworzy listę robotów, które zostaną rozmieszczone losowo na planszy.
         * @param count liczba robotów
         * @return metoda zwraca referencję <em>this</em>
         */
        public FieldBuilder setRobots(int count) {
            _robots = new LinkedList<>();
            Random gen = new Random();
            for (int i=0; i<count; ++i) {
                _robots.add(new Robot(gen.nextDouble()*_width, gen.nextDouble()*_height));
            }
            return this;
        }

        /**
         * Tworzy 3 nadajniki, rozmieszczone losowo na planszy.
         * @return metoda zwraca referencję <em>this</em>
         */
        public FieldBuilder setSignalSources() {
            _sources = new SignalSource[3];
            SignalSource.initFunctionParameters();
            Random gen = new Random();
            for (int i=0; i<3; ++i) {
                _sources[i] = new SignalSource(gen.nextDouble()*_width, gen.nextDouble()*_height);
            }
            return this;
        }

        /**
         * Wczytuje dane o obiektach na planszy z pliku.
         * @param file uchwyt do pliku
         * @return metoda zwraca referencję <em>this</em>
         * @throws IOException rzucany w przypadku błędu odczytu danych lub braku dostępu do pliku
         */
        public FieldBuilder setDataFromFile(File file) throws IOException {
            _sources = new SignalSource[3];
            _robots = new LinkedList<>();
            Scanner scan = new Scanner(file);
            _width = scan.nextInt();
            _height = scan.nextInt();
            scan.useLocale(Locale.US);
            int i = 0;
            while(scan.hasNextDouble()){
                double x_ = scan.nextDouble();
                double y_ = scan.nextDouble();
                System.out.println(x_+" "+y_);
                if(i >=0 && i < 3){
                    _sources[i] = new SignalSource(x_,y_);
                }
                else if(i == 4){
                    _mother = new Mother(x_,y_);
                }
                else{
                    _robots.add(new Robot(x_,y_));
                }
                i++;
            }
            return this;
        }

        /**
         * Tworzy obiekt klasy Field na podstawie ustawionych parametrów.
         * @return stworzony obiekt
         * @see #Field(int, int, Mother, List, SignalSource[])
         */
        public Field build() {
            return new Field(_width, _height, _mother, _robots, _sources);
        }
    }
}
