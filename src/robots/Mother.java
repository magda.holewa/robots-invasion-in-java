package robots;

import java.util.LinkedList;
import java.util.List;

/**
 * Klasa interpretująca robota-matkę.
 */
public class Mother extends Robot {
    private List<Robot> robots = new LinkedList<>();

    /**
     * Konstruktor parametryczny.
     * @param _x współrzędna pozioma robota
     * @param _y współrzędna pionowa robota
     */
    public Mother(double _x, double _y) {
        super(_x, _y);
    }

    /**
     * Dodaje robota do listy powiązanych robotów.
     * @param robot obiekt robota
     */
    public void addRobot(Robot robot) {
        robots.add(robot);
    }

    /**
     * Ustawia referencję do listy powiązanych robotów.
     * @param list lista robotów
     */
    public void setRobotsList(List<Robot> list) {
        robots = list;
    }

    /**
     * Zwraca referencję do listy powiązanych robotów.
     * @return lista robotów
     */
    public List<Robot> getRobots() {
        return robots;
    }

    /**
     * Oblicza, czy robot-matka znajduje się w trójkącie utworzonym przez źródła sygnału.
     * Przybliża pozycję nadajników za pomocą wartości sygnałów dla robotów znajdujących się najbliżej nich.
     * @return decyzja robota
     */
    public boolean isInTriangle() {
        boolean decision = true;

        Robot[] nearest = getNearestRobots();
        double[][] matrixData = new double[3][];
        for (int i = 0; i<3; ++i) {
            matrixData[i] = nearest[i].getSignals();
        }

        double base = (matrixData[0][0] + matrixData[1][1] + matrixData[2][2]) / 3.0;
        double a = Math.abs((matrixData[1][2] + matrixData[2][1]) / 2.0 - base);
        double b = Math.abs((matrixData[0][2] + matrixData[2][0]) / 2.0 - base);
        double c = Math.abs((matrixData[1][0] + matrixData[0][1]) / 2.0 - base);

        if (Math.abs(signals[0] - base) > Math.max(b, c) ||
                Math.abs(signals[1] - base) > Math.max(a, c) ||
                Math.abs(signals[2] - base) > Math.max(b, a)) {
            decision = false;
        }
        else {
            Matrix robotSignals = (new Matrix(matrixData)).transpose();

            //punkty trójkąta w stworzonej płaszczyznie
            double Ax = 0.0f;
            double Ay = 0.0f;
            double Bx = c;
            double By = 0.0f;
            double Cx = (c*c + b*b - a*a)/(2*c);
            double Cy = Math.sqrt(b*b - Cx*Cx);

            try {
                matrixData = new double[][]{{Ax, Bx, Cx}, {Ay, By, Cy}};
                Matrix points = new Matrix(matrixData);
                Matrix functionMatrix = points.multiply(robotSignals.inv());

                Matrix motherVector = new Matrix(3,1);
                for (int i=0; i<3; ++i) {
                    motherVector.set(i, 0, signals[i]);
                }

                Matrix transformedVector = functionMatrix.multiply(motherVector);
                double Dx = transformedVector.get(0,0);
                double Dy = transformedVector.get(1,0);

                decision = clockwiseOrder(Ax,Ay,Bx,By,Dx,Dy)
                        && clockwiseOrder(Bx,By,Cx,Cy,Dx,Dy)
                        && clockwiseOrder(Cx,Cy,Ax,Ay,Dx,Dy);
            }
            catch (Exception e) {
                decision = false;
            }
        }

        return decision;
    }

    /**
     * Oblicza, czy dane 3 punkty A, B, C w układzie kartezjańskim są podane w kolejności zgodnej z kierunkiem
     * wskazówek zegara.
     * @param ax współrzędna x punktu A
     * @param ay współrzędna y punktu A
     * @param bx współrzędna x punktu B
     * @param by współrzędna y punktu B
     * @param cx współrzędna x punktu C
     * @param cy współrzędna y punktu C
     * @return odpowiedź; <em>true</em> dla kierunku zgodnego ze wskazówkami zegara lub punktów współliniowych
     */
    private boolean clockwiseOrder(double ax, double ay, double bx, double by, double cx, double cy) {
        double[][] matrixData = {{ax, ay, 1}, {bx, by, 1}, {cx, cy, 1}};
        Matrix matrix = new Matrix(matrixData);
        double returnValue = -1.0;
        try {
            returnValue = matrix.det();
        }
        finally {
            return returnValue >= 0;
        }
    }

    /**
     * Znajduje roboty, które znajdują się najbliżej poszczególnych nadajników. Zakłada, że funkcja sygnału jest
     * funkcją rosnącą.
     * @return tablica robotów o rozmiarze równym liczbie nadajników
     */
    private Robot[] getNearestRobots() {
        Robot[] nearest = new Robot[3];
        for (int i = 0; i < 3; ++i) {
            nearest[i] = this;
        }
        for (Robot robot : robots) {
            for (int i = 0; i < 3; ++i) {
                if (robot.getSignals()[i] > nearest[i].getSignals()[i]) {
                    nearest[i] = robot;
                }
            }
        }
        return nearest;
    }
}
