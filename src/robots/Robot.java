package robots;

/**
 * Klasa reprezentująca robota zdolnego odbierać sygnały.
 */
public class Robot {
    protected double x;
    protected double y;
    protected double[] signals = new double[3];

    /**
     * Konstruktor parametryczny.
     * @param _x współrzędna pozioma robota
     * @param _y współrzędna pionowa robota
     */
    public Robot(double _x, double _y) {
        x = _x;
        y = _y;
    }

    /**
     * Zapisuje wartości funkcji sygnału dla pozycji robota.
     * @param signalSources tablica obiektów reprezentujących emiter sygnału
     */
    public void receiveSignals(SignalSource[] signalSources) {
        for (int i =0; i<signalSources.length; ++i) {
            signals[i] = signalSources[i].signalValue(x, y);
        }
    }

    /**
     * Zwraca tablicę wartości funkcji sygnału. Indeks elementu odpowiada indeksowi źródła sygnału
     * w tablicy przekazanej do metody <em>receiveSignals</em>.
     * @return tablica wartości funkcji
     * @see #receiveSignals(SignalSource[])
     */
    public double[] getSignals() {
        return signals;
    }

    /**
     * Zwraca współrzędną poziomą robota.
     * @return współrzędna pozioma
     */
    public double getX() {
        return x;
    }

    /**
     * Zwraca współrzędną pionową robota.
     * @return współrzędna pionowa
     */
    public double getY() {
        return y;
    }
}
