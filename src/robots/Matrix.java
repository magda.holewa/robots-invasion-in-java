package robots;

/**
 * Klasa reprezentująca rzeczywistą macierz prostokątną.
*/
public class Matrix {
    private double[] data;
    private int rows;
    private int cols;

    /**
     * Tworzy macierz z danymi z tablicy dwuwymiarowej. Brakujące wartości uzupełnia zerami.
     * @param d dwuwymiarowa tablica z danymi
     */
    public Matrix(double[][] d){
        int c = 0;
        for (int i=0;i<d.length;i++){
            c = Math.max(c, d[i].length);
        }

        this.cols = c;
        this.rows = d.length;
        data = new double[rows*cols];
        for(int i=0;i<d.length;++i) {
            for (int j = 0; j < this.cols; ++j) {
                data[i*cols+j] = d[i].length > j ? d[i][j] : 0;
            }
        }
    }

    /**
     * Tworzy macierz o zadanych wymiarach i uzupełnia ją zerami.
     * @param rows liczba wierszy
     * @param cols liczba kolumn
     */
    public Matrix(int rows, int cols){
        this.rows = rows;
        this.cols = cols;
        data = new double[rows*cols];
        for(int i=0;i<rows;++i) {
            for (int j = 0; j < cols; ++j) {
                data[i*cols+j] = 0;
            }
        }
    }

    /**
     * Konwertuje obiekt na tablicę dwuwymiarową.
     * @return  tablica dwuwymiarowa zawierająca elementy macierzy
     */
    public double[][] asArray(){
        double[][] ret = new double[rows][cols];

        for(int i=0;i<rows;++i){
            for(int j=0;j<cols;++j){
                ret[i][j] = data[i*cols+j];
            }
        }

        return ret;
    }

    /**
     * Pobiera zadany element macierzy.
     * @param r indeks wiersza (od 0)
     * @param c indeks kolumny (od 0)
     * @return Element macierzy <math>a<sub>ij</sub></math>
     */
    public double get(int r, int c){
        return data[r*cols+c];
    }

    /**
     * Ustawia wartość danego elementu macierzy.
     * @param r indeks wiersza (od 0)
     * @param c indeks kolumny (od 0)
     * @param value wartość elementu
     */
    public void set(int r, int c, double value){
        data[r*cols+c] = value;
    }

    /**
     * Zwraca liczbę wierszy macierzy.
     * @return liczba wierszy macierzy
     */
    public double getRows() {
        return rows;
    }

    /**
     * Zwraca liczbę kolumn macierzy.
     * @return liczba kolumn macierzy
     */
    public double getCols() {
        return cols;
    }

    /**
     * Dodaje macierz A i macierz B, przekazaną jako argument. Rzuca wyjątek w przypadku, gdy wymiary macierzy
     * są nieprawidłowe.
     * @param m macierz B
     * @return wynik dodawania A+B
     * @throws MatrixException macierze mają różne wymiary
     */
    public Matrix add(Matrix m) throws MatrixException{
        if(rows != m.rows || cols != m.cols)
            throw new MatrixException("Matrices must be of same size");

        Matrix ret = new Matrix(rows, cols);
        for(int i=0;i<rows*cols;++i){
            ret.data[i] = data[i] + m.data[i];
        }

        return ret;
    }

    /**
     * Odejmuje macierz B, przekazaną jako argument, od macierzy A. Rzuca wyjątek w przypadku, gdy wymiary macierzy
     * są nieprawidłowe.
     * @param m macierz B
     * @return wynik dodawania A-B
     * @throws MatrixException macierze mają różne wymiary
     */
    public Matrix sub(Matrix m) throws MatrixException {
        if(rows != m.rows || cols != m.cols)
            throw new MatrixException("Matrices must be of same size");

        Matrix ret = new Matrix(rows, cols);
        for(int i=0;i<rows*cols;++i){
            ret.data[i] = data[i] - m.data[i];
        }

        return ret;
    }

    /**
     * Mnoży macierz A przez skalar.
     * @param w skalar
     * @return wynik mnożenia w*A
     */
    public Matrix multiply(double w){
        Matrix ret = new Matrix(rows, cols);
        for(int i=0;i<rows*cols;++i)
            ret.data[i] = data[i] * w;
        return ret;
    }

    /**
     * Mnoży macierz A i macierz B, przekazaną jako argument. W przypadku nieodpowiednich wymiarów macierzy
     * wyrzucany jest wyjątek.
     * @param m macierz B
     * @return wynik mnożenia A*B
     * @throws MatrixException liczba kolumn macierzy A nie jest równa liczbie wierszy macierzy B
     */
    public Matrix multiply(Matrix m) throws MatrixException {
        if(cols != m.rows)
            throw new MatrixException("Incompatible matrix sizes");

        Matrix ret = new Matrix(rows, m.cols);
        for(int i=0;i<rows;++i){
            for(int j=0;j<m.cols;++j){
                ret.set(i,j,0);
                for(int k=0;k<cols;++k)
                    ret.set(i,j,ret.get(i,j)+get(i,k)*m.get(k,j));
            }
        }
        return ret;
    }

    /**
     * Tworzy macierz jednostkową o zadanym wymiarze
     * @param n wymiar macierzy
     * @return macierz jednostkowa n x n.
     */
    public static Matrix eye(int n){
        Matrix m = new Matrix(n, n);

        for(int i=0;i<n;++i){
            m.set(i,i,1);
        }

        return m;
    }

    /**
     * Wykonuje transpozycję macierzy.
     * @return macierz transponowana
     */
    public Matrix transpose() {
        Matrix ret = new Matrix(cols, rows);
        for (int i=0; i<rows; ++i) {
            for (int j=0; j<cols; ++j) {
                ret.set(j, i, get(i, j));
            }
        }
        return ret;
    }

    /**
     * Oblicza wyznacznik macierzy dla macierzy kwadratowej o wymiarze n<=3. Rzuca wyjątek w przypadku, gdy wymiary macierzy
     * są nieprawidłowe.
     * @return wyznacznik macierzy
     * @throws MatrixException macierz nie jest kwadratowa bądź ma zbyt duży wymiar
     */
    public double det() throws MatrixException{
        if (rows != cols) {
            throw new MatrixException("Incompatible matrix sizes");
        }
        if (rows == 1) {
            return data[0];
        }
        else if (rows == 2) {
            return data[0]*data[3]-data[1]*data[2];
        }
        else if (rows == 3) {
            double result = get(0,0)*get(1,1)*get(2,2);
            result += get(0,1)*get(1,2)*get(2,0);
            result += get(0,2)*get(1,0)*get(2,1);
            result -= get(0,2)*get(1,1)*get(2,0);
            result -= get(0,1)*get(1,0)*get(2,2);
            result -= get(0,0)*get(1,2)*get(2,1);
            return result;
        }
        else {
            throw new MatrixException("too big matrix");
        }
    }

    /**
     * Odwraca macierz kwadratową o wymiarze n >= 3. Rzuca wyjątek w przypadku, gdy wymiary macierzy
     * są nieprawidłowe.
     * @return macierz odwrotna
     * @throws MatrixException macierz nie jest kwadratowa bądź ma zbyt duży wymiar
     */
    public Matrix inv() throws MatrixException{
        if (rows != cols) {
            throw new MatrixException("Incompatible matrix sizes");
        }
        if (rows == 1) {
            Matrix matrix = new Matrix(1, 1);
            matrix.set(0,0, 1/data[0]);
            return matrix;
        }
        else if (rows == 2) {
            Matrix matrix = new Matrix(2,2);
            double invdet = 1.0/det();
            matrix.set(0,0, get(1,1)*invdet);
            matrix.set(0,1, (-1)*get(0,1)*invdet);
            matrix.set(1,0, (-1)*get(1,0)*invdet);
            matrix.set(1,1, get(0,0)*invdet);
            return matrix;
        }
        else if (rows == 3) {
            Matrix matrix = new Matrix(3,3);
            double invdet = 1.0/det();
            matrix.set(0, 1,(get(0, 2) * get(2, 1) - get(0, 1) * get(2, 2)) * invdet);
            matrix.set(0, 2,(get(0, 1) * get(1, 2) - get(0, 2) * get(1, 1)) * invdet);
            matrix.set(1, 0,(get(1, 2) * get(2, 0) - get(1, 0) * get(2, 2)) * invdet);
            matrix.set(1, 1,(get(0, 0) * get(2, 2) - get(0, 2) * get(2, 0)) * invdet);
            matrix.set(1, 2,(get(1, 0) * get(0, 2) - get(0, 0) * get(1, 2)) * invdet);
            matrix.set(2, 0,(get(1, 0) * get(2, 1) - get(2, 0) * get(1, 1)) * invdet);
            matrix.set(2, 1,(get(2, 0) * get(0, 1) - get(0, 0) * get(2, 1)) * invdet);
            matrix.set(2, 2,(get(0, 0) * get(1, 1) - get(1, 0) * get(0, 1)) * invdet);
            return matrix;
        }
        else {
            throw new MatrixException("too big matrix");
        }
    }

    /**
     * Zapisuje zawartość macierzy w postaci obiektu typu String.
     * @return łańcuch znaków z zawartością macierzy
     */
    public String toString() {
        StringBuilder buf = new StringBuilder();
        buf.append("[");
        for (int i = 0; i < rows; ++i) {
            if (i == 0)
                buf.append("[");
            else
                buf.append("\n[");
            for (int j = 0; j < cols; ++j) {
                buf.append(data[i * cols + j]);
                if (j != cols - 1)
                    buf.append(",");
            }
            buf.append("]");
            if (i != rows - 1)
                buf.append(",");
        }
        buf.append("]");

        return buf.toString();
    }
}
