# Robots invasion

Project for academic Java course. It's a JavaFX application which shows the solution for the following problem:

A group of robots and the Mother-robot wants to invade a planet. They have landed on the rectangular field, the location of each robot is random. 
There are 3 signal emitters on the field. The Mother has to figure out if she has landed inside the triangle made by these emitters. If she's not, all the robots have to escape.
None of the robots knows the location of any robot (including itself) but they know values of the signals and they can send them to the Mother.

The program may generate the field with all required elements or load data from file.